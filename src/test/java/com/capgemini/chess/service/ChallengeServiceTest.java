package com.capgemini.chess.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.chess.ChessApplication;
import com.capgemini.chess.dataaccess.dao.ChallengeDao;
import com.capgemini.chess.dataaccess.entities.Player;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ChessApplication.class)
public class ChallengeServiceTest {

	@Autowired
	ChallengeService service;
	
	@Autowired
	ChallengeDao challengeDao;
	
	@Test
	public void isFindingAllChallenges(){
		//given
		int expectedChallengeListSize = 11;
		//when
		int actualChallengeListSize = service.findAll().size();
		//then
		Assert.assertEquals(expectedChallengeListSize, actualChallengeListSize);
	}
	
	@Test
	public void isFindingChallengesByNickNameIfExist(){
		//given
		int expectedChallengesQuantity = 2;
		String expectedPlayerNickName = "Daredevil";
		//when
		int actualChallengesQuantity = challengeDao.findByPlayerNickName("Daredevil").size();
		//then
		Assert.assertEquals(expectedChallengesQuantity, actualChallengesQuantity);
	}
	
	@Test
	public void isFindingChallengesByNickNameIfNotExist(){
		//given
		int expectedChallengesQuantity = 0;
		//when
		int actualChallengesQuantity = challengeDao.findByPlayerNickName("UnknownPlayer").size();
		//then
		Assert.assertEquals(expectedChallengesQuantity, actualChallengesQuantity);
	}
	
	@Test
	public void isSavingChallenge(){
		//given
		int expectedChallengQuantity = 12;
		//when
		challengeDao.saveChallenge("Daredevil", "Hulk");
		int actualChallengesQuantity = challengeDao.findAll().size();
		//then
		Assert.assertEquals(expectedChallengQuantity, actualChallengesQuantity);
	}
}
