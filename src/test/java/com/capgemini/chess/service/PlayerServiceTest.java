package com.capgemini.chess.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.chess.ChessApplication;
import com.capgemini.chess.dataaccess.dao.PlayerDao;
import com.capgemini.chess.dataaccess.dto.PlayerDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ChessApplication.class)
public class PlayerServiceTest {

	@Autowired
	private PlayerService service;
	@Autowired
	private PlayerDao playerDao;

	@Test
	public void isExistingPlayerFoundOnTheList() {

		// given
		PlayerDto playerExpected = new PlayerDto("Rambo", "John", "Rambo", 1, 1, 1);

		// when
		PlayerDto playerActual = service.showPlayerInfo("Rambo");

		// then
		Assert.assertEquals(playerExpected, playerActual);
	}

	@Test
	public void isNonExistingPlayerNotFoundOnTheList() {

		// given
		PlayerDto playerExpected = new PlayerDto("Rambo", "James", "Rambo", 1, 1, 1);

		// when
		PlayerDto playerActual = service.showPlayerInfo("Rambo");

		// then
		Assert.assertNotEquals(playerExpected, playerActual);
	}

	@Test
	public void isSavingPlayerWorks() {

		// given
		PlayerDto playerExpected = new PlayerDto("Kowal", "Jan", "Kowalski", 6, 6, 6);

		// when
		playerDao.savePlayer(playerExpected);

		PlayerDto playerActual = service.showPlayerInfo("Kowal");

		// then
		Assert.assertEquals(playerExpected, playerActual);
	}

	@Test
	public void isFindingAllPlayers() {

		// given
		int expectedPlayerDataBaseSize = 14;

		// when
		int actualPlayerDataBaseSize = service.findAllPlayers().size();

		// then
		Assert.assertEquals(expectedPlayerDataBaseSize, actualPlayerDataBaseSize);
	}

	@Test
	public void isFindingByLevelIfPlayerExists() {
		// given
		int expectedNumberOfPlayersLevelTen = 3;

		// when
		int actualNumberOfPlayersLevelTen = service.findByLevel(10).size();

		// then
		Assert.assertEquals(expectedNumberOfPlayersLevelTen, actualNumberOfPlayersLevelTen);
	}

	@Test
	public void isFindingByLevelIfPlayerDoesntExist() {
		// given
		int expectedNumberOfPlayersLevelThree = 0;
		// when
		int actualNumberOfPlayersLevelThree = service.findByLevel(3).size();
		// then
		Assert.assertEquals(expectedNumberOfPlayersLevelThree, actualNumberOfPlayersLevelThree);

	}

	@Test
	public void isFindingByScoreIfPlayerExist() {
		// given
		int expectedNumberOfPlayersWithScoreEightyThousand = 1;
		// when
		int actualNumberOfPlayersWithScoreEightyThousand = service.findByScore(80000).size();
		// then
		Assert.assertEquals(expectedNumberOfPlayersWithScoreEightyThousand,
				actualNumberOfPlayersWithScoreEightyThousand);

	}

	@Test
	public void isFindingByScoreIfPlayerDoesntExist() {
		// given
		int expectedNumberOfPlayersWithScoreEightyThousandAndOne = 0;
		// when
		int actualNumberOfPlayersWithScoreEightyThousandAndOne = service.findByScore(80001).size();
		// then
		Assert.assertEquals(expectedNumberOfPlayersWithScoreEightyThousandAndOne,
				actualNumberOfPlayersWithScoreEightyThousandAndOne);
	}

	@Test
	public void isFindingByNumberOfGamesIfPlayerExist() {
		// given
		int expectedNumberOfPlayersWithOnePlayedGame = 3;
		// when
		int actualNumberOfPlayersWithOnePlayedGame = service.findByPlayedGamesNumber(1).size();
		// then
		Assert.assertEquals(expectedNumberOfPlayersWithOnePlayedGame, actualNumberOfPlayersWithOnePlayedGame);

	}
	
	@Test
	public void isFindingByNumberOfGamesIfPlayerDoesntExist(){
		// given
		int expectedNumberOfPlayerWithTwoPlayedGames = 0;
		//when
		int actualNumberOfPlayerWithTwoPlayedGames = service.findByPlayedGamesNumber(2).size();
		//then
		Assert.assertEquals(expectedNumberOfPlayerWithTwoPlayedGames, actualNumberOfPlayerWithTwoPlayedGames);
		
	}
}
