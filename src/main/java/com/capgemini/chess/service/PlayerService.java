package com.capgemini.chess.service;

import java.util.List;

import com.capgemini.chess.dataaccess.dto.PlayerDto;

public interface PlayerService {
	
	List<PlayerDto> findAllPlayers();
	
	List<PlayerDto> findByNickName(String nickName);
	
	List<PlayerDto> findByLevel(Integer Level);
	
	List<PlayerDto> findByScore(Integer Score);
	
	List<PlayerDto> findByPlayedGamesNumber(Integer playedGamesNumber);
	
	PlayerDto showPlayerInfo(String nickName);
}
