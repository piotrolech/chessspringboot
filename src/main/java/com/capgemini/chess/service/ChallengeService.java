package com.capgemini.chess.service;

import java.util.List;

import com.capgemini.chess.dataaccess.dto.ChallengeDto;

public interface ChallengeService {
	List<ChallengeDto> findAll();
	
	List<ChallengeDto> findByPlayerNickName(String playerNickName);
	
	List<ChallengeDto> generateAutomaticChallengeList(String playerNickName);
	
	List<ChallengeDto> sendChallenge(String senderNickName, String receiverNickname);
	
	int saveChallenge(String senderNickName, String receiverNickName);
	
}
