package com.capgemini.chess.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.dataaccess.dao.ChallengeDao;
import com.capgemini.chess.dataaccess.dao.PlayerDao;
import com.capgemini.chess.dataaccess.dto.ChallengeDto;
import com.capgemini.chess.service.ChallengeService;

@Service
public class ChallengeServiceImpl implements ChallengeService {
	
	@Autowired
	ChallengeDao challengeDao;
	
	@Autowired
	PlayerDao playerDao;

	@Override
	public List<ChallengeDto> findAll() {
		return challengeDao.findAll();
	}

	@Override
	public List<ChallengeDto> findByPlayerNickName(String playerNickName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ChallengeDto> generateAutomaticChallengeList(String playerNickName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ChallengeDto> sendChallenge(String senderNickName, String receiverNickname) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int saveChallenge(String senderNickName, String receiverNickName) {
		// TODO Auto-generated method stub
		return 0;
	}

}
