package com.capgemini.chess.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.dataaccess.dao.PlayerDao;
import com.capgemini.chess.dataaccess.dto.PlayerDto;
import com.capgemini.chess.service.PlayerService;

@Service
public class PlayerServiceImpl implements PlayerService{
	
	@Autowired
	private PlayerDao playerDao;

	public List<PlayerDto> findAllPlayers() {
		return playerDao.findAll();
	}

	public List<PlayerDto> findByNickName(String nickName) {
		List<PlayerDto> playerDtos = new ArrayList<>();
		if(nickName != null){
			playerDtos = playerDao.findByNickName(nickName);
		}
		return playerDtos;
	}

	public List<PlayerDto> findByLevel(Integer level) {
		List<PlayerDto> playerDtos = new ArrayList<>();
		if(level != null){
			playerDtos = playerDao.findByLevel(level);
		}
		return playerDtos;
	}

	public List<PlayerDto> findByScore(Integer score) {
		List<PlayerDto> playerDtos = new ArrayList<>();
		if(score != null){
			playerDtos = playerDao.findByScore(score);
		}
		return playerDtos;
	}

	public List<PlayerDto> findByPlayedGamesNumber(Integer playedGamesnumber) {
		List<PlayerDto> playerDtos = new ArrayList<>();
		if(playedGamesnumber != null){
			playerDtos = playerDao.findByPlayedGamesNumber(playedGamesnumber);
		}
		return playerDtos;
	}

	public PlayerDto showPlayerInfo(String nickName) {
		List<PlayerDto> playerDtos = new ArrayList<>();
		if(nickName != null){
			playerDtos = playerDao.findByNickName(nickName);
			return playerDtos.get(0);
		}
		return null;
	}
}
