package com.capgemini.chess.dataaccess.entities;

public class Player {
	
	private String nickName;
	private String name;
	private String surname;
	private Integer level;
	private Integer score;
	private Integer numberOfGamesPlayed;
	
	public Player(){
		
	}
	
	public Player(String nickName, String name, String surname) {
		this.nickName = nickName;
		this.name = name;
		this.surname = surname;
		this.level = 1;
		this.score = 0;
		this.numberOfGamesPlayed = 0;
	}

	public Player(String nickName, String name, String surname, Integer level, Integer score, Integer numberOfGamesPlayed) {
		this.nickName = nickName;
		this.name = name;
		this.surname = surname;
		this.score = score;
		this.numberOfGamesPlayed = numberOfGamesPlayed;
		if(level < 1 || level > 10){
			throw new IllegalArgumentException("Level must be between 0 and 10");
		}else{
			this.level = level;
		}
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Integer getNumberOfGamesPlayed() {
		return numberOfGamesPlayed;
	}

	public void setNumberOfGamesPlayed(int numberOfGamesPlayed) {
		this.numberOfGamesPlayed = numberOfGamesPlayed;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nickName == null) ? 0 : nickName.hashCode());
		result = prime * result + ((numberOfGamesPlayed == null) ? 0 : numberOfGamesPlayed.hashCode());
		result = prime * result + ((score == null) ? 0 : score.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (level == null) {
			if (other.level != null)
				return false;
		} else if (!level.equals(other.level))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nickName == null) {
			if (other.nickName != null)
				return false;
		} else if (!nickName.equals(other.nickName))
			return false;
		if (numberOfGamesPlayed == null) {
			if (other.numberOfGamesPlayed != null)
				return false;
		} else if (!numberOfGamesPlayed.equals(other.numberOfGamesPlayed))
			return false;
		if (score == null) {
			if (other.score != null)
				return false;
		} else if (!score.equals(other.score))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}
	
	
}
