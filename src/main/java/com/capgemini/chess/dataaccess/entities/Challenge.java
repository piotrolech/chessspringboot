package com.capgemini.chess.dataaccess.entities;

public class Challenge {
	private String senderNickName;
	private String receiverNickName;
	private boolean isSent;
	private boolean isAccepted;

	public Challenge(String senderNickName, String receiverNickName) {
		if (senderNickName == null || receiverNickName == null) {
			throw new IllegalArgumentException("Challenge needs two players. Neither of them can be null");
		}
		this.setSenderNickName(senderNickName);
		this.setReceiverNickName(receiverNickName);
		this.isSent = false;
		this.isAccepted = false;
	}

	public String getReceiverNickName() {
		return receiverNickName;
	}

	public void setReceiverNickName(String receiverNickName) {
		this.receiverNickName = receiverNickName;
	}

	public String getSenderNickName() {
		return senderNickName;
	}

	public void setSenderNickName(String senderNickName) {
		this.senderNickName = senderNickName;
	}

	public boolean isSent() {
		return isSent;
	}

	public void setSent(boolean isSent) {
		this.isSent = isSent;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

}
