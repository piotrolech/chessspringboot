package com.capgemini.chess.dataaccess.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.capgemini.chess.dataaccess.dto.PlayerDto;
import com.capgemini.chess.dataaccess.entities.Player;

public class PlayerMapper {
	
	public static PlayerDto getPlayerDtoFromPlayer(Player player){
		PlayerDto playerDto = null;
		if(player != null){
			playerDto = new PlayerDto();
			playerDto.setNickName(player.getNickName());
			playerDto.setName(player.getName());
			playerDto.setSurname(player.getSurname());
			playerDto.setLevel(player.getLevel());
			playerDto.setScore(player.getScore());
			playerDto.setNumberOfGamesPlayed(player.getNumberOfGamesPlayed());
		}
		return playerDto;
	}

	public static List<PlayerDto> getPlayersDtoFromPlayers(List<Player> players){
		return players.stream().map(p -> getPlayerDtoFromPlayer(p)).collect(Collectors.toList());
	}
	
	public static Player getPlayerFromPlayerDto(PlayerDto playerDto){
		Player player = null;
		if(playerDto != null){
			player = new Player();
			player.setNickName(playerDto.getNickName());
			player.setName(playerDto.getName());
			player.setSurname(playerDto.getSurname());
			player.setLevel(playerDto.getLevel());
			player.setScore(playerDto.getScore());
			player.setNumberOfGamesPlayed(playerDto.getNumberOfGamesPlayed());
		}
		return player;
	}
	
}
