package com.capgemini.chess.dataaccess.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.capgemini.chess.dataaccess.dto.ChallengeDto;
import com.capgemini.chess.dataaccess.entities.Challenge;

public class ChallengeMapper {
	public static ChallengeDto getChallengeDtoFromChallenge(Challenge challenge) {
		ChallengeDto challengeDto = null;
		if(challenge != null){
			challengeDto = new ChallengeDto();
			challengeDto.setSenderNickName(challenge.getSenderNickName());
			challengeDto.setReceiverNickName(challenge.getReceiverNickName());
			challengeDto.setSent(challenge.isSent());
			challengeDto.setAccepted(challenge.isAccepted());
		}
		return challengeDto;
	}
	
	public static List<ChallengeDto> getChallengeDtosListFromChallengeList(List<Challenge> challengeList){
		return challengeList.stream().map(c ->getChallengeDtoFromChallenge(c)).collect(Collectors.toList());
	}
}
