package com.capgemini.chess.dataaccess.dto;

public class ChallengeDto {
	private String senderNickName;
	private String receiverNickName;
	private boolean isSent;
	private boolean isAccepted;

	public ChallengeDto() {
		super();
	}

	public ChallengeDto(String senderNickName, String receiverNickName) {
		super();
		this.senderNickName = senderNickName;
		this.receiverNickName = receiverNickName;
	}

	public String getReceiverNickName() {
		return receiverNickName;
	}

	public void setReceiverNickName(String receiverNickName) {
		this.receiverNickName = receiverNickName;
	}

	public String getSenderNickName() {
		return senderNickName;
	}

	public void setSenderNickName(String senderNickName) {
		this.senderNickName = senderNickName;
	}

	public boolean isSent() {
		return isSent;
	}

	public void setSent(boolean isSent) {
		this.isSent = isSent;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isAccepted ? 1231 : 1237);
		result = prime * result + (isSent ? 1231 : 1237);
		result = prime * result + ((receiverNickName == null) ? 0 : receiverNickName.hashCode());
		result = prime * result + ((senderNickName == null) ? 0 : senderNickName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChallengeDto other = (ChallengeDto) obj;
		if (isAccepted != other.isAccepted)
			return false;
		if (isSent != other.isSent)
			return false;
		if (receiverNickName == null) {
			if (other.receiverNickName != null)
				return false;
		} else if (!receiverNickName.equals(other.receiverNickName))
			return false;
		if (senderNickName == null) {
			if (other.senderNickName != null)
				return false;
		} else if (!senderNickName.equals(other.senderNickName))
			return false;
		return true;
	}

}
