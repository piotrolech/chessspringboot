package com.capgemini.chess.dataaccess.dao;
import java.util.List;

import com.capgemini.chess.dataaccess.dto.*;

public interface ChallengeDao {
	/**
	 * Method returns all challenges from database
	 * @return
	 */
	
	List<ChallengeDto> findAll();
	
	/**
	 * Method returns challenges list for specified player searched by players nickname
	 * @param playerNickName
	 * @return
	 */
	
	List<ChallengeDto> findByPlayerNickName(String playerNickName);
	
	/**
	 * 
	 * @param playerNickName
	 * @return
	 */
	
	/**
	 * Saves created challenge to database
	 * @param senderNickName
	 * @param receiverNickName
	 * @return
	 */
	int saveChallenge(String senderNickName, String receiverNickName);

}
