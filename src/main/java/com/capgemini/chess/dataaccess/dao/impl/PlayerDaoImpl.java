package com.capgemini.chess.dataaccess.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.capgemini.chess.dataaccess.dao.PlayerDao;
import com.capgemini.chess.dataaccess.dto.PlayerDto;
import com.capgemini.chess.dataaccess.entities.Player;
import com.capgemini.chess.dataaccess.mapper.PlayerMapper;

@Component
public class PlayerDaoImpl implements PlayerDao{
	private List<Player> ALL_PLAYERS = new ArrayList<Player>();
	
	public PlayerDaoImpl(){
		addTestPlayers();
	}

	@Override
	public List<PlayerDto> findAll() {
		return PlayerMapper.getPlayersDtoFromPlayers(ALL_PLAYERS);
	}
	@Override
	public List<PlayerDto> findByNickName(String nickName) {
		List<Player> players = ALL_PLAYERS.stream().filter(p -> p.getNickName().equals(nickName)).collect(Collectors.toList());
		return PlayerMapper.getPlayersDtoFromPlayers(players);
	}
	@Override
	public List<PlayerDto> findByLevel(Integer level) {
		List<Player> players = ALL_PLAYERS.stream().filter(p -> p.getLevel().equals(level)).collect(Collectors.toList());
		return PlayerMapper.getPlayersDtoFromPlayers(players);
	}
	@Override
	public List<PlayerDto> findByScore(Integer score) {
		List<Player> players = ALL_PLAYERS.stream().filter(p -> p.getScore().equals(score)).collect(Collectors.toList());
		return PlayerMapper.getPlayersDtoFromPlayers(players);
	}
	@Override
	public List<PlayerDto> findByPlayedGamesNumber(Integer playedGamesNumber){
		List<Player> players = ALL_PLAYERS.stream().filter(p -> p.getNumberOfGamesPlayed().equals(playedGamesNumber)).collect(Collectors.toList());
		return PlayerMapper.getPlayersDtoFromPlayers(players);
	}
	@Override
	public int savePlayer(PlayerDto player) {
		if(player!= null){
			List<Player> players = ALL_PLAYERS.stream().filter(p -> p.getNickName().equals(player.getNickName())).collect(Collectors.toList());
			if(players.isEmpty()){
				ALL_PLAYERS.add(PlayerMapper.getPlayerFromPlayerDto(player));
				return 0;
			}
		}
		return -1;
	}
	private void addTestPlayers() {
		ALL_PLAYERS.add(new Player("Rambo", "John", "Rambo", 1, 1, 1));
		ALL_PLAYERS.add(new Player("Deadpool", "Wade", "Wilson", 1, 54, 1));
		ALL_PLAYERS.add(new Player("IronMan", "Tony", "Stark", 2, 306, 1));
		ALL_PLAYERS.add(new Player("Dr Strange", "Steven", "Strange", 2, 700, 36));
		ALL_PLAYERS.add(new Player("Captain America", "Steve", "Rogers", 4,1230,58));
		ALL_PLAYERS.add(new Player("Spawn", "Al", "Simmons", 4, 2231, 46));
		ALL_PLAYERS.add(new Player("Spiderman", "Peter", "Parker", 5, 2402, 81));
		ALL_PLAYERS.add(new Player("CrazyJohn","John" ,"Smith", 6, 4802, 126));
		ALL_PLAYERS.add(new Player("IronFist", "Danny", "Rand", 7, 9602, 185));
		ALL_PLAYERS.add(new Player("Hulk", "Bruce", "Banner", 8, 19205, 247));
		ALL_PLAYERS.add(new Player("Thor", "Thor", "Son of Odin", 9, 31467, 335));
		ALL_PLAYERS.add(new Player("Sielomastah", "Piotr", "Olech", 10, 76809, 407));
		ALL_PLAYERS.add(new Player("Daredevil", "Matt", "Murdock",10,80000,510));
		ALL_PLAYERS.add(new Player("Punisher", "Frank", "Castle", 10,99678,678));
	}
}
