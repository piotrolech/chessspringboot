package com.capgemini.chess.dataaccess.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.capgemini.chess.dataaccess.dao.ChallengeDao;
import com.capgemini.chess.dataaccess.dto.ChallengeDto;
import com.capgemini.chess.dataaccess.entities.Challenge;
import com.capgemini.chess.dataaccess.mapper.ChallengeMapper;

@Component
public class ChallengeDaoImpl implements ChallengeDao {
	private List<Challenge> ALL_CHALLENGES = new ArrayList<Challenge>();

	public ChallengeDaoImpl() {
		createTestChallengeDatabase();
	}

	private void createTestChallengeDatabase() {
		ALL_CHALLENGES.add(new Challenge("Rambo", "Deadpool"));
		ALL_CHALLENGES.add(new Challenge("Daredevil", "Punisher"));
		ALL_CHALLENGES.add(new Challenge("Punisher", "Daredevil"));
		ALL_CHALLENGES.add(new Challenge("Spawn", "IronFist"));
		ALL_CHALLENGES.add(new Challenge("Spawn", "Deadpool"));
		ALL_CHALLENGES.add(new Challenge("Rambo", "IronFist"));
		ALL_CHALLENGES.add(new Challenge("Thor", "Hulk"));
		ALL_CHALLENGES.add(new Challenge("Spiderman", "IronMan"));
		ALL_CHALLENGES.add(new Challenge("IornMan", "Hulk"));
		ALL_CHALLENGES.add(new Challenge("Spawn", "Hulk"));
		ALL_CHALLENGES.add(new Challenge("Spawn", "Spiderman"));
	}

	@Override
	public List<ChallengeDto> findAll() {
		return ChallengeMapper.getChallengeDtosListFromChallengeList(ALL_CHALLENGES);
	}

	@Override
	public List<ChallengeDto> findByPlayerNickName(String playerNickName) {
		List<Challenge> challengesSentByPlayer = ALL_CHALLENGES.stream()
				.filter(p -> p.getSenderNickName().equals(playerNickName)).collect(Collectors.toList());
		List<Challenge> challengeReceivedByPlayer = ALL_CHALLENGES.stream()
				.filter(p -> p.getReceiverNickName().equals(playerNickName)).collect(Collectors.toList());
		List<Challenge> challenges = Stream.concat(challengesSentByPlayer.stream(), challengeReceivedByPlayer.stream())
				.collect(Collectors.toList());
		return ChallengeMapper.getChallengeDtosListFromChallengeList(challenges);
	}

	@Override
	public int saveChallenge(String senderNickName, String receiverNickName) {
		if((senderNickName != null) && (receiverNickName != null)){
			Challenge challenge = new Challenge(senderNickName,receiverNickName);
			challenge.setSent(true);
			ALL_CHALLENGES.add(new Challenge(senderNickName, receiverNickName));
			return 0;
		}
		return -1;
	}

}
