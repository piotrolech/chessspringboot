package com.capgemini.chess.dataaccess.dao;

import java.util.List;

import com.capgemini.chess.dataaccess.dto.*;

public interface PlayerDao {

	/**
	 * Returns list of all players in database
	 * 
	 * @return
	 */
	List<PlayerDto> findAll();

	/**
	 * Returns list of players with one player on it. Searched by nickname.
	 * @param nickName
	 * @return
	 */
	List<PlayerDto> findByNickName(String nickName);

	/**
	 * Returns list of players with specified level
	 * @param level
	 * @return
	 */
	List<PlayerDto> findByLevel(Integer level);

	/**
	 * Returns list of players with specified score
	 * @param score
	 * @return
	 */
	List<PlayerDto> findByScore(Integer score);

	/**
	 * Returns list of players with specified number of played games
	 * @param playedGamesNumber
	 * @return
	 */
	List<PlayerDto> findByPlayedGamesNumber(Integer playedGamesNumber);

	/**
	 * Saves new player to database
	 * @param playerDto
	 * @return
	 */
	int savePlayer(PlayerDto playerDto);
}
